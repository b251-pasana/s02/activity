year = int(input("Please enter a year: "))

if(year <= 0):
    print("Please enter a valid input! Value cannot be less than or equal to 0.")
elif(year % 4 == 0) and (year % 100 != 0): # perfectly divisible by 4 and not a century year (!divisible by 100)
    print(f"'{year}' is a leap year")
elif (year % 100 == 0) and (year % 400 == 0): # a century year (divisible by 100) and perfectly divisible by 400
    print(f"'{year}' is a leap year")
else:    
    print (f"'{year}' is not a leap year")


row = int(input("Enter the number of rows: "))
col = int(input("Enter the number of column: "))

for num in range(row): 
    print("* " * col) # '*' multiply by column

